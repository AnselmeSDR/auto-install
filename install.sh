#!/bin/bash

echo -e "\n\033[31mMise à jour du système\033[0m\n"
apt-get install sudo apt update
apt-get upgrade
echo -e "\033[1;32m Mise à jour du système réussi \033[0m \n"

echo -e "\n \033[31m Installation de MySQL \033[0m \n"
apt-get install mysql-client mysql-server
echo -e "\033[1;32m Installation MySQL réussi \033[0m \n"

echo -e "\n \033[31m Installation de Git \033[0m \n"
apt-get install git
echo -e "\033[1;32m Installation Git réussi \033[0m \n"

echo -e "\n \033[31m Installation de phpMyAdmin \033[0m \n"
apt-get install phpmyadmin
ln -s /usr/share/phpmyadmin/ /var/www/html/phpmyadmin
echo -e "\033[1;32m Installation phpMyAdmin réussi \033[0m \n"

echo -e "\n \033[31m Installation de xz-utils \033[0m \n"
apt-get install xz-utils
echo -e "\033[1;32m Installation xz-utils réussi \033[0m \n"

echo -e "Récupération des fichiers\n"
wget https://runtime.fivem.net/artifacts/fivem/build_proot_linux/master/651-9979540ed3eb68238b0caa079d8b6352b1c28c16/fx.tar.xz
tar xvfJ fx.tar.xz
rm fx.tar.xz
git clone https://gitlab.com/AnselmeSDR/auto-install.git
mv alpine ./auto-install
mv run.sh ./auto-install
rm ./auto-install/ProjetHub_Mystralia_PDF.pdf
read -p 'Entrer le nom de votre serveur: ' dossier
mv auto-install $dossier

chmod -R 777 *

echo -e "\033[31mVotre serveur est presque prêt, modifiez dans le fichier server.cfg\n\033[0m"
read -p 'Appuyez sur entrée pour valider' -s toto
nano ./$dossier/server.cfg
echo -e "\n\nUtilisez la commande suivante pour démarrer le serveur: bash run.sh +exec server.cfg\n"
